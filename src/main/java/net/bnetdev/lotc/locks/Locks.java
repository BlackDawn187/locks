/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.locks;

import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.extensions.Extension;
import net.bnetdev.lotc.files.Configuration;
import net.bnetdev.lotc.locks.commands.CommandManager;
import net.bnetdev.lotc.locks.commands.LockCommand;
import net.bnetdev.lotc.locks.listeners.ListenerManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Locks extends JavaPlugin {

    private LOTC lotc;
    private Locks plugin;

    private Extension ext;
    private Configuration configuration;

    private CommandManager cManager;
    private ListenerManager lManager;

    @Override
    public void onEnable() {
        this.plugin = this;

        if (Bukkit.getPluginManager().getPlugin("LOTC") != null) {
            this.lotc = (LOTC) Bukkit.getPluginManager().getPlugin("LOTC");
            int result = this.lotc.getExtensionManager().add(this, "1.0");
            if (result == 0) {
                this.ext = this.lotc.getExtensionManager().get(this.getName());
                this.ext.getDataFolder();
                this.configuration = this.ext.getConfiguration("Configuration.ini");
                this.cManager = new CommandManager(this.plugin);
                this.lManager = new ListenerManager(this.plugin);

            } else {
                this.lotc.getChatManager().log("Hook: Locks Failed");
            }
        } else {
            this.getLogger().severe("LOTC Plugin Framework unavailable!");
        }
    }

    @Override
    public void onDisable() {
        if (Bukkit.getPluginManager().getPlugin("LOTC") != null) {
            for (int i = 0; i < this.cManager.getCommands().size(); i++) {
                this.ext.unregisterCommand(this.cManager.getCommands().get(i));
            }
        }
    }

    public CommandManager getCommandManager() {
        return this.cManager;
    }
    
    public Extension asExtension() {
        return this.ext;
    }
    
    public LOTC getFrameWork() {
        return this.lotc;
    }
    
}
