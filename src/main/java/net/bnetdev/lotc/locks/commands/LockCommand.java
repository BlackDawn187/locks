/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.locks.commands;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import net.bnetdev.lotc.LOTC;
import net.bnetdev.lotc.commands.AbstractCommand;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class LockCommand extends AbstractCommand {

    private static final String name = "lock";
    private ArrayList<Object> materials = new ArrayList<>();
    private ArrayList<EntityType> entities = new ArrayList<>();

    public LockCommand(LOTC instance) {
        super(instance, name);

        this.materials.add(Material.CHEST);
        this.materials.add(Material.TRAPPED_CHEST);
        this.materials.add(Material.FURNACE);
        this.materials.add(Material.BURNING_FURNACE);
        this.materials.add(Material.BREWING_STAND);
        this.materials.add(Material.DISPENSER);
        this.materials.add(Material.HOPPER);
        this.materials.add(Material.DROPPER);
        this.materials.add(Material.TRAP_DOOR);
        this.materials.add(Material.WOODEN_DOOR);
        this.materials.add(Material.IRON_DOOR);
        this.materials.add(Material.CAULDRON);
        this.materials.add(Material.FENCE);
        this.materials.add(Material.FENCE_GATE);
        this.materials.add(Material.WOOD_BUTTON);
        this.materials.add(Material.STONE_BUTTON);
        this.materials.add(Material.WOOD_PLATE);
        this.materials.add(Material.STONE_PLATE);
        this.materials.add(Material.GOLD_PLATE);
        this.materials.add(Material.IRON_PLATE);
        this.materials.add(Material.LEVER);

        this.entities.add(EntityType.ARMOR_STAND);
        this.entities.add(EntityType.MINECART_CHEST);
        this.entities.add(EntityType.MINECART_FURNACE);
        this.entities.add(EntityType.MINECART_HOPPER);
        this.entities.add(EntityType.PAINTING);
        this.entities.add(EntityType.ITEM_FRAME);
    }

    @Override
    public void execute(Player p, String var) {
        LOTC.getInstance().getChatManager().broadcast(p, "Executed Locks Command.");

        boolean result = this.isEntity(p);
        if (result) {
            LOTC.getInstance().getChatManager().broadcast(p, "The specified entity is lockable.");
        } else {
            boolean result2 = this.isContainer(p.getTargetBlock((Set<Material>) null, 6));
            if (result2) {
                LOTC.getInstance().getChatManager().broadcast(p, "The specified block is lockable.");
            } else {
                LOTC.getInstance().getChatManager().broadcast(p, "The specified block/entity is not lockable.");
            }
        }
    }

    @Override
    public void execute(Player p, String var, String var2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean isContainer(Block b) {
        return this.materials.contains(b.getType());
    }

    public boolean isEntity(Player p) {
        for (Entity e : p.getWorld().getEntities()) {
            if (e.getLocation() == p.getEyeLocation()) {
                if (this.entities.contains(e.getType())) {
                    return true;
                }
            }
        }

        return false;
    }

}
