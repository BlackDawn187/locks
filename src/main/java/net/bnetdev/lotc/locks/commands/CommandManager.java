/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.locks.commands;

import java.util.ArrayList;
import net.bnetdev.lotc.commands.AbstractCommand;
import net.bnetdev.lotc.locks.Locks;

public class CommandManager {
 
    private Locks plugin;
    private ArrayList<AbstractCommand> commands = new ArrayList<>();
    
    private LockCommand lock;
    
    public CommandManager(Locks instance) {
        this.plugin = instance;
        
        this.lock = new LockCommand(this.plugin.getFrameWork());
        this.commands.add(this.lock);
              
        for (AbstractCommand command : this.commands) {
            this.plugin.asExtension().registerCommand(command);
        }
    }
    
    public ArrayList<AbstractCommand> getCommands() {
        return this.commands;
    }
    
}
