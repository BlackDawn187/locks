/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotc.locks.listeners;

import net.bnetdev.lotc.locks.Locks;
import org.bukkit.Bukkit;

public class ListenerManager {

    private Locks plugin;
    
    public ListenerManager(Locks instance) {
        this.plugin = instance;
        
        Bukkit.getPluginManager().registerEvents(new CommandPreprocess(this.plugin), this.plugin);
    }    
}
